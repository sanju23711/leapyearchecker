package com.leapyear.test;

import org.junit.Assert;
import org.junit.Test;

import com.leapyear.LeapYearApplication;

/**
 * The Class LeapYearApplicationTest.
 */
public class LeapYearApplicationTest {

  /** The leap year. */
  private Integer leapYear = 2020;

  /** The not leap year. */
  private Integer notLeapYear = 2019;

  @Test
  public void testSuccess() {
    String responseLike = "This year " + leapYear + " is a Leap Year";
    Assert.assertEquals(responseLike, LeapYearApplication.CheckingLeapYear(leapYear));
  }

  @Test
  public void testFailed() {
    String responseLike = "This year " + notLeapYear + " is not a Leap Year";
    Assert.assertEquals(responseLike, LeapYearApplication.CheckingLeapYear(notLeapYear));
  }

  @Test
  public void testUserInput() {
    Assert.assertNotNull(LeapYearApplication.CheckingLeapYear(null));
  }
}
