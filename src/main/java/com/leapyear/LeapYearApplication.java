/**
 * 
 */
package com.leapyear;

import java.util.Scanner;

/**
 * @author Admin
 *
 */
public class LeapYearApplication {

  /**
   * @param args
   */
  public static void main(String[] args) {
    CheckingLeapYear(null);
  }

  /**
   * Checking leap year.
   *
   * @param yearNumber
   *          the year number
   */
  public static String CheckingLeapYear(final Integer year) {
    Integer userEnterYear = year;
    if (year == null) {
      @SuppressWarnings("resource")
      Scanner scanner = new Scanner(System.in);
      System.out.print("Please enter the year ::");
      userEnterYear = scanner.nextInt();
    }
    Boolean flag;
    if (userEnterYear % 400 == 0 || userEnterYear % 4 == 0) {
      flag = Boolean.TRUE;
    } else if (userEnterYear % 100 == 0) {
      flag = Boolean.FALSE;
    } else {
      flag = Boolean.FALSE;
    }
    String response;
    if (flag) {
      response = "This year " + userEnterYear + " is a Leap Year";
    } else {
      response = "This year " + userEnterYear + " is not a Leap Year";
    }
    System.out.println("Response :: " + response);
    return response;
  }
}
